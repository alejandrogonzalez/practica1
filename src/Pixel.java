import java.util.Scanner;

public class Pixel {
    int x, y, r ,g ,b;
    public ColorRGB colorRGB;

    public static void main(String[] args) throws ColorException {
        Pixel miPixel = new Pixel();
        miPixel.Pixel(0,0,0,0,0);
    }
    public void Pixel(int x, int y,int r, int g,int b) throws ColorException{

        boolean bucle = false;
        do{
            try{
                Scanner lector = new Scanner(System.in);

                System.out.println("Introduce tres valores");
                System.out.println("Valor rojo: ");
                r = lector.nextInt();
                System.out.println("Valor verde: ");
                g = lector.nextInt();
                System.out.println("Valor azul: ");
                b = lector.nextInt();
                System.out.println("Introduce dos valores de posición");
                System.out.println("Introduce valor de x: ");
                x = lector.nextInt();
                System.out.println("Introduce valor de y: ");
                y = lector.nextInt();

                if (r < 0 || r > 255){
                    throw new ColorException("Color rojo",r);
                }else if (g < 0 || g > 255){
                    throw new ColorException("Color verde",g);
                }else if (b < 0 || b > 255){
                    throw new ColorException("Color azul",b);
                }else{
                    this.r = r;
                    this.g = g;
                    this.b = b;
                    bucle = true;


                }
            }catch (ColorException e){
                System.out.println("ERROR, FUERA DE RANGO" + e.getMessage());

            }

        }while (!bucle);





    }
    public void asignarPosicion(int x, int y){
        this.x = x;
        this.y = y;

    }

    public void asignarPosicionX(int x){
        this.x = x;

    }
    public void asignarPosicionY(int y){
        this.y = y;

    }
    public int[] obtenerPosicion(){
        int[] arrayPosicion = {x,y};
        return arrayPosicion;
    }

    public int obtenerPosicionX(){
        return x;
    }

    public int obtenerPosicionY(){
        return y;
    }

    public void asignarColor(int r, int g, int b) throws ColorException{
        if (r < 0 || r > 255){
            throw new ColorException("Color rojo",r);
        }else if (g < 0 || g > 255){
            throw new ColorException("Color verde",g);
        }else if (b < 0 || b > 255){
            throw new ColorException("Color azul",b);
        }else{
            this.r = r;
            this.g = g;
            this.b = b;


        }

    }

    public int[] obtenerColor(){
        int [] arrayColor = {r,g,b};
        return arrayColor;
    }

    public void asignarColorR(int r) throws ColorException{
        if (r < 0 || r > 255){
            throw new ColorException("Color rojo",r);
        }else{
            this.r = r;
        }
    }

    public void asignarColorG(int g) throws ColorException{
        if (g < 0 || g > 255){
            throw new ColorException("Color verde",g);
        }else{
            this.g = g;
        }

    }

    public void asignarColorB(int b) throws ColorException{
        if (b < 0 || b > 255){
            throw new ColorException("Color azul",b);
        }else{
            this.b = b;
        }

    }

    public int obtenerColorR(){
        return r;
    }

    public int obtenerColorG(){
        return g;
    }

    public int obtenerColorB(){
        return b;
    }

}
