import java.util.Scanner;

public class ColorRGB {
    private int r,g,b;

    public static void main(String[] args) throws ColorException {
        ColorRGB miColorRGB = new ColorRGB();
        miColorRGB.ColorRGB(0,0,0);

    }

    public void ColorRGB(int r, int g, int b) throws ColorException{

        boolean bucle = false;
        do{
            try{
                Scanner lector = new Scanner(System.in);

                System.out.println("Introduce tres valores");
                System.out.println("Valor rojo: ");
                r = lector.nextInt();
                System.out.println("Valor verde: ");
                g = lector.nextInt();
                System.out.println("Valor azul: ");
                b = lector.nextInt();

                if (r < 0 || r > 255){
                    throw new ColorException("Color rojo",r);
                }else if (g < 0 || g > 255){
                    throw new ColorException("Color verde",g);
                }else if (b < 0 || b > 255){
                    throw new ColorException("Color azul",b);
                }else{
                    this.r = r;
                    this.g = g;
                    this.b = b;
                    bucle = true;

                }
            }catch (ColorException e){
                System.out.println("ERROR, FUERA DE RANGO" + e.getMessage());

            }

        }while (!bucle);

    }






    public void asignarColor(int r, int g, int b) throws ColorException{
        if (r < 0 || r > 255){
            throw new ColorException("Color rojo",r);
        }else if (g < 0 || g > 255){
            throw new ColorException("Color verde",g);
        }else if (b < 0 || b > 255){
            throw new ColorException("Color azul",b);
        }else{
            this.r = r;
            this.g = g;
            this.b = b;

        }

    }

    public int[] obtenerColor(){
        int [] arrayColor = {r,g,b};
        return arrayColor;
    }

    public void asignarColorR(int r) throws ColorException{
        if (r < 0 || r > 255){
            throw new ColorException("Color rojo",r);
        }else{
            this.r = r;
        }
    }

    public void asignarColorG(int g) throws ColorException{
        if (g < 0 || g > 255){
            throw new ColorException("Color verde",g);
        }else{
            this.g = g;
        }

    }

    public void asignarColorB(int b) throws ColorException{
        if (b < 0 || b > 255){
            throw new ColorException("Color azul",b);
        }else{
            this.b = b;
        }

    }

    public int obtenerColorR(){
        return r;
    }

    public int obtenerColorG(){
        return g;
    }

    public int obtenerColorB(){
        return b;
    }
}
